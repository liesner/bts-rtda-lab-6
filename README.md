## Downloading laboratory boilerplate

- Clone repo an move to downloaded folder
 
```terminal
git clone https://gitlab.com/liesner/bts-rtda-lab-6.git
cd bts-rtda-lab-6
```

## Data sets

- The data is a zip file shared at, [dataset](https://drive.google.com/file/d/1d_FiCCJCXeSkB8weofaQFdO7q3HSw9o4/view?usp=sharing).
- The origin of the data is [https://www.kaggle.com/retailrocket/ecommerce-dataset](https://www.kaggle.com/retailrocket/ecommerce-dataset)  
 
Download and unzip the dataset on ```bts-rtda-lab-6/data/dataset``` 

## Practical activity

## Running spark container

- Build and start spark container

```
docker-compose build
docker-compose run spark bash
```

- load ```/appdata/data/dataset/events.csv``` dataset

```
val events = spark.read.option("header", "true").csv("/appdata/data/dataset/events.csv")
```

- Using the events DataFrame an DataFrame api resolve the following problems:

    - Count the total count of events.
    - Using DataFrame api count the total number of [view, transaction, addtocart] events
    - Using SQL DataFrame count the total number of [view, transaction, addtocart] events.
        - Hint: Create a temporal view 
        ```
            events.createOrReplaceTempView("events")
            spark.sql("sql query over events")
       ```
    - Using DataFrame api count the total number of uniques visitors.
    - Using DataFrame api show a list with 10 visitor who buy more items.
    - Using SQL DataFrame show a list with 10 visitor who buy more items.
    
## Assignment

- Download the [Stack Overflow 2019 Developer Survey](https://drive.google.com/open?id=1QOmVDpd8hcVYqqUXDXf68UMDWQZP0wQV). The data set contain the following files:
    
    - ```survey_results_schema.csv```: contain a description of each one of the columns on ```survey_results_public.csv``` each one of them representing a question.    
    - ```survey_results_public.csv```: contain the answers of each one of the participants on the survey.  
    - ```so_survey_2019.pdf```: contain the full description of the survey.
        
- Using the spark-shell and DataFrame API (the docker environment used on class) process the file ```survey_results_schema.csv``` to answer the following questions:
    
    - How many programmers participated in the survey?
    - How many developers answer "Yes, it's part of our process" on the question "Does your company regularly employ unit tests in the development of their products?".
    - How many woman have being coding form more than 20 years.   
    - Percentage of developers how do open source "Less than once a month but more than once per year", "Once a month or more often", "Never" or "Less than once per year".
    - Developers count by Role and Gender.
    


